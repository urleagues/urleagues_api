package terraform_testing

import (
	"log"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestTerraformDigitalOceanDropletCreation(t *testing.T) {
	assert := assert.New(t)
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../terraform",
	})

	defer terraform.Destroy(t, terraformOptions)

	stdout := terraform.InitAndApply(t, terraformOptions)
	resourceCount := terraform.GetResourceCount(t, stdout)
	assert.Equal(2, resourceCount.Add)

	dropletStatus := terraform.Output(t, terraformOptions, "urleagues_droplet_status")
	log.Printf("droplet status: %s", dropletStatus)
	assert.Equal("active", dropletStatus)
}
