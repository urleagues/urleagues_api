resource "digitalocean_droplet" "urleagues" {
	image  = "ubuntu-23-10-x64"
  name   = "urleagues-api-1"
  region = "sfo3"
  size   = "s-1vcpu-1gb"
	ssh_keys = [
		data.digitalocean_ssh_key.tim-mac.id,
		data.digitalocean_ssh_key.gitlab-cicd.id,
	]
}

data "digitalocean_domain" "pandascores" {
  name = "pandascores.com"
}

resource "digitalocean_record" "api" {
  domain = data.digitalocean_domain.pandascores.id
  type = "A"
  name = "api"
  value = digitalocean_droplet.urleagues.ipv4_address
}

data "digitalocean_project" "urleagues_project" {
	name = "Urleagues"
}

resource "digitalocean_project_resources" "urleagues" {
	project = data.digitalocean_project.urleagues_project.id
	resources = [
		digitalocean_droplet.urleagues.urn
	]
}

output "urleagues_droplet_status" {
	value = digitalocean_droplet.urleagues.status
}
