package main

import (
	"crypto/tls"
	"log"
	"net"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"

	"gitlab.com/urleagues_api/app_features/environment_management"
	"gitlab.com/urleagues_api/app_features/games"
	"gitlab.com/urleagues_api/app_features/logging"
)

func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	statusResponse := struct {
		Value string `json:"status"`
	}{
		Value: "OK",
	}

	render.JSON(w, r, statusResponse)
}

func routes() *chi.Mux {
	router := chi.NewRouter()
	corsHandler := cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		//AllowedOrigins: []string{"http://localhost:3000"},
		AllowedOrigins: []string{"https://www.pandascores.com"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"Link", "Access-Control-Allow-Origin"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	router.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.Compress(5),
		middleware.RedirectSlashes,
		middleware.Recoverer,
		corsHandler,
	)

	router.Route("/v1", func(r chi.Router) {
		r.Get("/api/healthcheck", healthCheck)
		r.Mount("/api/games", games.Routes())
	})

	return router
}

func redirectToTls(serverPort string) {
	server := http.Server{
		Addr: ":80",
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			host, port, _ := net.SplitHostPort(r.Host)
			if host == "" || port == "" {
				host = r.Host
			}
			u := r.URL
			u.Host = net.JoinHostPort(host, serverPort)
			u.Scheme = "https"
			http.Redirect(w, r, u.String(), http.StatusMovedPermanently)
		}),
	}
	log.Fatal(server.ListenAndServe())
}

func main() {
	environment_management.LoadEnvironmentVariables()
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Preparing to start GoLang Urleagues API server")

	router := routes()

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route) // This walks all of the routes and prints out each route and the method accepted by it
		logger.Info().Msgf("%s %s\n", method, route)
		return nil
	}

	if err := chi.Walk(router, walkFunc); err != nil {
		logger.Panic().Msgf("Logging err: %s\n", err.Error())
		log.Panicf("Logging err: %s\n", err.Error()) // Panic if there's an error while walking the routes
	}

	serverPort := environment_management.ReadFromEnv("URLEAGUES_API_SERVER_PORT", "8080")
	environment := environment_management.ReadFromEnv("URLEAGUES_ENV", "development")

	if environment == "test" {
		logger.Info().Msgf("Starting server on port %s", serverPort)
		log.Fatal(http.ListenAndServe(":"+serverPort, router))
	}

	tlsCertPath := environment_management.ReadFromEnv("URLEAGUES_API_TLS_CERT", "server.crt")
	tlsKeyPath := environment_management.ReadFromEnv("URLEAGUES_API_TLS_KEY", "server.key")

	// Read TLS Files in from environment.
	tlsCert, err := tls.LoadX509KeyPair(tlsCertPath, tlsKeyPath)
	if err != nil {
		log.Fatalf("Failed to load TLS X509 key pair: %v", err)
	}

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
	}

	server := &http.Server{
		Addr:      ":" + serverPort,
		Handler:   router,
		TLSConfig: tlsConfig,
	}

	go redirectToTls(serverPort)

	logger.Info().Msgf("Starting server on port %s", serverPort)
	log.Fatal(server.ListenAndServeTLS("", ""))
}
