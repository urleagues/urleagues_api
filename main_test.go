package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/urleagues_api/app_features/data_management/data_models"
	"gitlab.com/urleagues_api/app_features/environment_management"
)

var serverHostname string
var serverPort string
var serverURLPrefix string
var serverScheme string

func setup() *exec.Cmd {
	environment_management.LoadEnvironmentVariables()

	serverPort = environment_management.ReadFromEnv("URLEAGUES_API_SERVER_PORT", "8080")
	serverURLPrefix = environment_management.ReadFromEnv("URLEAGUES_API_SERVER_URL_PREFIX", "/v1/api")
	serverHostname = environment_management.ReadFromEnv("URLEAGUES_API_SERVER_HOSTNAME", "localhost")
	serverScheme = environment_management.ReadFromEnv("URLEAGUES_API_SCHEME", "http://")

	cmd := exec.Command("go", "run", "./main.go")
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}

	if serverHostname == "" || serverHostname == "localhost" {
		serverHostname = "localhost"
		log.Printf("Running urleagues_api server locally for development integration tests")
		err := cmd.Start()
		if err != nil {
			log.Fatal("Error starting urleagues_api server locally for integration tests", err)
		}
	}

	time.Sleep(3 * time.Second)

	return cmd
}

func teardown(cmd *exec.Cmd) {
	log.Printf("Tearing down api server after integration testing")
	if err := syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL); err != nil {
		log.Fatal("Failed to terminate urleagues_api server: ", err)
	}
}

func mainTesting(m *testing.M) int {
	serverCommand := setup()
	defer teardown(serverCommand)
	return m.Run()
}

func TestMain(m *testing.M) {
	os.Exit(mainTesting(m))
}

func TestHealthCheck(t *testing.T) {
	t.Log("Testing health check")

	tests := []struct {
		name           string
		url            string
		acceptHeader   string
		expectedResult string
	}{
		{
			name:           "Success Expected",
			acceptHeader:   "application/json",
			url:            "/healthcheck",
			expectedResult: "{\"status\":\"OK\"}\n",
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			URL := fmt.Sprintf("%s%s:%s%s%s", serverScheme, serverHostname, serverPort, serverURLPrefix, testStruct.url)
			t.Logf("URL passed: %s", URL)
			resp, err := http.Get(URL)

			if err != nil {
				t.Errorf("Error making request to urleagues_api server: %v", err)
			}
			defer resp.Body.Close()

			assert := assert.New(t)
			assert.Equal(resp.StatusCode, http.StatusOK)

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Errorf("Error reading response body from request: %v", err)
			}
			assert.Equal(testStruct.expectedResult, string(body))
		})
	}
}

func TestCreateGame(t *testing.T) {
	t.SkipNow()
	t.Log("Doing create game integration test")
	tests := []struct {
		name              string
		url               string
		contentTypeHeader string
		body              data_models.Game
		expectedResult    data_models.Game
	}{
		{
			name:              "Game Creation Success Expected",
			url:               "/games",
			contentTypeHeader: "application/json",
			body: data_models.Game{
				Entity1:      "WaterBuffalo",
				Entity1Score: 7,
				Entity2:      "Tiger",
				Entity2Score: 4,
			},
			expectedResult: data_models.Game{
				Entity1:      "waterbuffalo",
				Entity1Score: 7,
				Entity2:      "tiger",
				Entity2Score: 4,
			},
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			postBody, _ := json.Marshal(testStruct.body)
			URL := fmt.Sprintf("%s%s:%s%s%s", serverScheme, serverHostname, serverPort, serverURLPrefix, testStruct.url)
			t.Logf("URL passed: %s", URL)
			resp, err := http.Post(URL, testStruct.contentTypeHeader, bytes.NewBuffer(postBody))
			if err != nil {
				t.Errorf("Error making request to urleagues_api server: %v", err)
			}
			defer resp.Body.Close()

			if status := resp.StatusCode; status != http.StatusOK {
				t.Errorf("handler returned wrong status code: got %v want %v",
					status, http.StatusOK)
			}

			/*
				Does the same as the implementation below, almost identically,
				but may be more complicated for cognitive overhead. It's definitely
				something to look into later
			*/
			// buf := new(bytes.Buffer)
			// buf.ReadFrom(resp.Body)
			// respBody := buf.String()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Errorf("Error reading response body from request: %v", err)
			}
			var responseGame data_models.Game
			unMarshalErr := json.Unmarshal(body, &responseGame)
			if unMarshalErr != nil {
				t.Errorf("Response object could not be unmarshalled into a game object: %v", err)
			}

			assert := assert.New(t)
			assert.Equal(testStruct.expectedResult.Entity1, responseGame.Entity1, "Entity1 should match")
			assert.Equal(testStruct.expectedResult.Entity1Score, responseGame.Entity1Score, "Entity1Score should match")
			assert.Equal(testStruct.expectedResult.Entity2, responseGame.Entity2, "Entity2 should match")
			assert.Equal(testStruct.expectedResult.Entity2Score, responseGame.Entity2Score, "Entity2Score should match")
			assert.GreaterOrEqual(responseGame.Id, 0, "Returned game id should be greater or equal to 0")
		})
	}
}
