# urleagues_api

API Server Code for urleagues. A free, modern, intuitive platform for managing games and sports with your friends.

## Starting server

In the root directory running `go run main.go` starts the server in the foreground and blocks the shell.
During development, manual testing with Curl is recommended to determine if routes are functioning correctly.

## Sqlite Management

To get a view of the current contents of the sqlite database use either the
sqlite shell directly or use the command line tool `sqlite-utils`.

sqlite-utils command for viewing database contents:

`sqlite-utils leagues.db "select * from soccer_games" --table`

## Manual API Testing with Curl

Health Check Testing (GET Request to healthcheck endpoint):

```
curl http://localhost:8080/v1/api/healthcheck \
	-H "Accept: application/json"
```

Create Game Testing (POST Request to games endpoint):

```
curl -X POST http://localhost:8080/v1/api/games \
	-H "Accept: application/json" \
	-H "Content-Type: application/json" \
	-d '{"entity1": "Naruto", "entity2": "Goofy", "entity1score": 9, "entity2score": 0}'
```

Read Game Testing (GET Request to games endpoint with an additional game ID in the URL Param):

```
curl http://localhost:8080/v1/api/games/9851 \
	-H "Accept: application/json"
```

Read All Games Testing (GET Request to games endpoint with)

```
curl http://localhost:8080/v1/api/games \
	-H "Accept: application/json"
```

Update Game Testing (PUT Request to games endpoint with an additional game ID in the URL Param):

```
curl -X PUT http://localhost:8080/v1/api/games/9853 \
	-H "Accept: application/json" \
	-H "Content-Type: application/json" \
	-d '{"entity1": "Luigi", "entity2": "Bowser", "entity1score": 4, "entity2score": 3}'
```

Delete Game Testing (DELETE Request to games endpoint with an additional game ID in the URL Param):

```
curl -X DELETE http://localhost:8080/v1/api/games/451 \
	-H "Accept: application/json"
```



## Automated Testing

In the root of the project directory, run `go test`. The results will printed to the shell.
If all is well then you'll see `PASS` in the output. Otherwise the specific failures and reasons will be
printed to the shell.

To run all tests in all directories, run `go test ./... -v`. This will run the test files in the sub packages as well
as the root package. You'll see the output of each package and whether or not it has tests during this test run.

### Killing Remaining Go Processes When Test Cleanup Fails

Find Process IDs (PID) of the running Go application with (There will be two processes opened): `ps -a`

Kill the running go processes with the PIDs you just found with (Will have to be run twice): `kill <pid>`

## Project Structure/Consistency

### Code Consistency

Each new set of routes for a particular feature set in the API gets its own package. For instance the games API routes
are all within their own package along with their tests.

Each tooling feature goes into its own package. For example we have error_handling, logging, and data_management all within their
own packages so that it's very easy to understand which portion of the application is doing what.

The route methods are the entry points for client interaction with the codebase, and they are triggering the heavy lifting portions of the
codebase to perform the necessary actions to resolve client requests.

### Logging

Logging is handled in this API with the Zerolog logging package: [Zerolog](https://github.com/rs/zerolog)

The Zerolog logger is configured inside of the logging package in this codebase.

Each custom package in the module is then importing the custom logging package and using the DefaultConfigLogger() to get access
to outputting their logs to a rotating log file.

For now this is enough, but when this gets extended it will include labels for each custom package so tracking where logs are
emitted is fast with low cognitive overhead.

Additionally, there will be a separate log file for the error logLevel and above.

ZeroLog log levels:
* panic (zerolog.PanicLevel, 5)
* fatal (zerolog.FatalLevel, 4)
* error (zerolog.ErrorLevel, 3)
* warn (zerolog.WarnLevel, 2)
* info (zerolog.InfoLevel, 1)
* debug (zerolog.DebugLevel, 0)
* trace (zerolog.TraceLevel, -1)

### Error Handling

Error handling will be done simply through the use of the Go standard library [errors](https://pkg.go.dev/errors) package.

Error handling is done wherever the error occurs to make sure whatever problem arises gets resolved immediately.

For error reponses to the client the error_handling package is where the pre-canned response types are stored.
It follows the error responses that [go-chi](https://github.com/go-chi/chi) uses in their http server example:
[server_example](https://github.com/go-chi/chi/blob/master/_examples/rest/main.go).

If the route function needs to be notified of an error deeper in the stack without throwing the error all the way up,
channels will be used if necessary.

For more in depth error handling discussion and more guidance this application will follow Earthly's guidance in their [error handling](https://earthly.dev/blog/golang-errors/) blog post.

### Data Modeling

All data models for the application are placed into the `data_models` sub package of `data_management`.

Inside of the data_management package the data_models can be imported with the `.` notation so
that there's easy access to the structures without too much unnecessary context.

Outside of the data_management package it's important to have the context provided so the import
will omit the `.` notation in order to use it's full package name.
