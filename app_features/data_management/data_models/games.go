package data_models

import (
	"net/http"
)

// Game is a struct for holding game objects
type Game struct {
	Id           int    `json:"id"`
	Entity1      string `json:"entity1"`
	Entity2      string `json:"entity2"`
	Entity1Score int    `json:"entity1Score"`
	Entity2Score int    `json:"entity2Score"`
}

// go-chi interface implementation
func (game *Game) Bind(r *http.Request) error {
	// Pre-processing before unmarshalling values from network data to struct object
	return nil
}

// go-chi interface implementation
func (game *Game) Render(w http.ResponseWriter, r *http.Request) error {
	// Pre-processing before struct values are marshalled into a response and sent across the wire.
	return nil
}

// Games is a struct for holding a slice of game objects
type Games struct {
	Games []Game `json:"games"`
}

// go-chi interface implementation
func (games *Games) Bind(r *http.Request) error {
	// Pre-processing before unmarshalling values from network data to struct object
	return nil
}

// go-chi interface implementation
func (games *Games) Render(w http.ResponseWriter, r *http.Request) error {
	// Pre-processing before struct values are marshalled into a response and sent acress the wire.
	return nil
}
