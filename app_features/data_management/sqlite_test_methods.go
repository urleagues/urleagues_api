package data_management

import (
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/urleagues_api/app_features/data_management/data_models"
	"gitlab.com/urleagues_api/app_features/logging"
)

var INITIAL_DB_VALUES = Games{
	Games: []Game{
		{
			Entity1:      "mario",
			Entity2:      "bowser",
			Entity1Score: 4,
			Entity2Score: 3,
		},
		{
			Entity1:      "luigi",
			Entity2:      "wario",
			Entity1Score: 5,
			Entity2Score: 2,
		},
		{
			Entity1:      "yoshi",
			Entity2:      "waluigi",
			Entity1Score: 3,
			Entity2Score: 1,
		},
	},
}

const INITIAL_DB_ROWS_AFFECTED int = 3

func TestSetup() (string, error) {

	tempDir, err := os.MkdirTemp("./", "test_files")
	if err != nil {
		log.Fatal("Error creating temp test directory")
		return "", err
	}
	os.Setenv("URLEAGUES_PROJECT_DIRECTORY", tempDir)
	logger := logging.DefaultConfigLogger()
	logger.Info().Msgf("Temp directory for testing: %s", tempDir)
	logger.Info().Msg("Setting up initial values in test database")
	insertErr := TestDatabaseInsert()
	if insertErr != nil {
		logger.Fatal().Msgf("Failed to insert records into test database: %v", insertErr)
		return "", insertErr
	}
	return tempDir, nil
}

func TestDatabaseReset(expectedRowsAffected int, t *testing.T) error {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Resetting test database to original state")
	sqliteConnector := NewSqliteConnection()
	rowsAffected, err := sqliteConnector.SqliteDeleteAllGames()
	if err != nil {
		logger.Error().Err(err).Msg("Error executing Delete all records from test database")
		return err
	}

	assert.Equal(t, expectedRowsAffected, rowsAffected)

	insertionErr := TestDatabaseInsert()
	if insertionErr != nil {
		logger.Error().Err(insertionErr).Msg("Error inserting records into test database during reset")
		return err
	}

	return nil
}

func TestDatabaseInsert() error {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to insert test data into test database")
	sqliteConnector := NewSqliteConnection()
	rowsAffected, err := sqliteConnector.SqliteInsertMultipleGames(INITIAL_DB_VALUES)
	if err != nil {
		logger.Error().Err(err).Msg("Error executing insert query into test database")
		return err
	}

	if rowsAffected != INITIAL_DB_ROWS_AFFECTED {
		log.Fatalf("Wanted to insert %d rows, but actually affected %d rows in the database", INITIAL_DB_ROWS_AFFECTED, rowsAffected)
	}

	return nil
}

func TestDatabaseValidation(games Games, t *testing.T) {
	sqliteConnector := NewSqliteConnection()
	databaseGames, err := sqliteConnector.SqliteReadAllGames()
	if err != nil {
		t.Error("Error reading all the games from the test database")
	}

	for index, game := range databaseGames.Games {
		assert := assert.New(t)
		assert.Equal(games.Games[index].Entity1, game.Entity1, "Entity1 should match")
		assert.Equal(games.Games[index].Entity1Score, game.Entity1Score, "Entity1Score should match")
		assert.Equal(games.Games[index].Entity2, game.Entity2, "Entity2 should match")
		assert.Equal(games.Games[index].Entity2Score, game.Entity2Score, "Entity2Score should match")
		assert.GreaterOrEqual(game.Id, 0, "Returned game id should be greater or equal to 0")
	}
}
