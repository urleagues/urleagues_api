package data_management

import (
	"errors"

	"gitlab.com/urleagues_api/app_features/logging"

	. "gitlab.com/urleagues_api/app_features/data_management/data_models"
)

// Something that was found earlier was that you can use channels to inform the route functions about whether or not an error has occurred deep in the stack.
// Would be interesting to toss channels around and get used to them. Passing an error channel deep into the feature code could be a great way for route
// functions to handle errors without escalating them back up the stack.

func InsertGame(game Game) (int, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to insert a game with the data access wrapper")
	sqliteConnector := NewSqliteConnection()
	dbInsertId, err := sqliteConnector.SqliteInsertGame(game)
	if err != nil {
		logger.Error().Err(err).Msg("Failed to insert game into the data source with the data access wrapper")
		return 0, err
	}

	if dbInsertId < 0 {
		logger.Warn().Msg("The attempt to insert game into the data source with the data access wrapper failed to modify any records")
		return 0, errors.New("Insert query didn't modify any errors in the database")
	}

	return dbInsertId, nil
}

// Need to figure out how to handle return types that will be nil
func ReadGame(id int) (Game, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to read a game with the data access wrapper")
	sqliteConnector := NewSqliteConnection()
	game, err := sqliteConnector.SqliteReadGame(id)
	if err != nil {
		logger.Error().Err(err).Msg("Failed to read game from the data source with the data access wrapper")
		return Game{}, err
	}

	return game, nil
}

func ReadGames() (Games, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to read all games from the data source with the data access wrapper")
	sqliteConnector := NewSqliteConnection()
	games, err := sqliteConnector.SqliteReadAllGames()
	if err != nil {
		logger.Error().Err(err).Msg("Failed to read all games from the data source with the data access wrapper")
		return Games{}, err
	}

	return *games, nil
}

func UpdateGame(game Game) error {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to update game in the data source from the data access wrapper")
	sqliteConnector := NewSqliteConnection()
	dbResponse, err := sqliteConnector.SqliteUpdateGame(game)
	if err != nil {
		logger.Error().Err(err).Msg("Failed to update the game in the data source with the data access wrapper")
		return err
	}

	if dbResponse < 0 {
		logger.Warn().Msg("The attempt to update the game in the data source with the data access wrapper failed to modify any records")
		return errors.New("Update query didn't modify any errors in the database")
	}

	return nil
}

func DeleteGame(id int) error {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to delete a game from the data source with the data access wrapper")
	sqliteConnector := NewSqliteConnection()
	dbResponse, err := sqliteConnector.SqliteDeleteGame(id)
	if err != nil {
		logger.Error().Err(err).Msg("Failed to delete the game in the data source with the data access wrapper")
		return err
	}

	if dbResponse < 0 {
		logger.Warn().Msg("The attempt to delete the game in the data source with the data access wrapper failed to modify any records")
		return errors.New("Delete query didn't modify any errors in the database")
	}

	return nil
}
