package data_management

import (
	"fmt"
	"path"

	. "gitlab.com/urleagues_api/app_features/data_management/data_models"
	"gitlab.com/urleagues_api/app_features/environment_management"
	"gitlab.com/urleagues_api/app_features/logging"

	"database/sql"
	"sync"

	_ "github.com/mattn/go-sqlite3"
)

const CREATE_TABLE = `
	CREATE TABLE IF NOT EXISTS [soccer_games] (
	id INTEGER NOT NULL PRIMARY KEY,
	entity1 TEXT NOT NULL,
	entity2 TEXT NOT NULL,
	entity1_score INTEGER NOT NULL,
	entity2_score INTEGER NOT NULL
	);`

var once sync.Once

type SqliteConnector struct {
	db *sql.DB
}

var sqliteConnector *SqliteConnector

func NewSqliteConnection() *SqliteConnector {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Client attempting to retrieve Sqlite Connector singleton")
	if sqliteConnector == nil {
		logger.Info().Msg("Creating singleton Sqlite connection")
		once.Do(
			func() {
				project_directory := environment_management.ReadFromEnv("URLEAGUES_PROJECT_DIRECTORY", "./")
				db_file_name := environment_management.ReadFromEnv("URLEAGUES_SQLITE_DB_FILE_NAME", "leagues.db")
				db_file_path := path.Join(project_directory, db_file_name)
				db, err := sql.Open("sqlite3", db_file_path)
				if err != nil {
					logger.Error().Err(err).Msg("Error opening the Sqlite database file.")
				}
				if _, err := db.Exec(CREATE_TABLE); err != nil {
					logger.Error().Err(err).Msg("Error creating the initial database table for soccer games.")
				}
				sqliteConnector = &SqliteConnector{db: db}
			})
	} else {
		logger.Info().Msg("Sqlite connection singleton already created. No operation performed.")
	}

	return sqliteConnector
}

func (sqliteConnector *SqliteConnector) SqliteInsertGame(game Game) (int, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to insert game data into Sqlite DB")
	logger.Info().Msgf("Game details: %s %d ; %s %d", game.Entity1, game.Entity1Score, game.Entity2, game.Entity2Score)
	res, err := sqliteConnector.db.Exec("INSERT INTO soccer_games VALUES(NULL, ?, ?, ?, ?)", game.Entity1, game.Entity2, game.Entity1Score, game.Entity2Score)
	if err != nil {
		logger.Error().Err(err).Msg("Error executing INSERT query into Sqlite Database")
		return 0, err
	}

	var dbResponseId int64
	if dbResponseId, err = res.LastInsertId(); err != nil {
		logger.Error().Err(err).Msg("Error retrieving last insert id from Sqlite INSERT query Exec")
		return 0, err
	}
	return int(dbResponseId), nil
}

func (sqliteConnector *SqliteConnector) SqliteInsertMultipleGames(games Games) (int, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to insert multiple games into the Sqlite DB")
	logger.Info().Msg("Game Details:")
	insertQuery := "INSERT INTO soccer_games VALUES "
	for index, game := range games.Games {
		logger.Info().Msgf("%s %d ; %s %d", game.Entity1, game.Entity1Score, game.Entity2, game.Entity2Score)

		insertQuery += fmt.Sprintf("(NULL, '%s', '%s', %d, %d)", game.Entity1, game.Entity2, game.Entity1Score, game.Entity2Score)

		if index < len(games.Games)-1 {
			insertQuery += ", "
		} else {
			insertQuery += ";"
		}
	}

	res, err := sqliteConnector.db.Exec(insertQuery)
	if err != nil {
		logger.Error().Err(err).Msgf("Error doing batch insert into Sqlite DB")
		return 0, err
	}

	var dbResponseId int64
	if dbResponseId, err = res.RowsAffected(); err != nil {
		logger.Error().Err(err).Msg("Error retrieving rows affected from Sqlite multiple INSERT query Exec")
		return 0, err
	}

	return int(dbResponseId), nil
}

func (sqliteConnector *SqliteConnector) SqliteReadGame(id int) (Game, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msgf("Attempting to read single game data from Sqlite DB. Game ID Passed: %d", id)

	// Query DB row based on ID
	row := sqliteConnector.db.QueryRow("SELECT * FROM soccer_games WHERE id=?", id)

	// Parse row into Game struct
	game := Game{}
	var err error
	if err = row.Scan(&game.Id, &game.Entity1, &game.Entity2, &game.Entity1Score, &game.Entity2Score); err == sql.ErrNoRows {
		logger.Error().Err(err).Msg("Error retrieving game from Sqlite database")
		return Game{}, err
	}

	logger.Info().Msgf("Game retrieved from database: %v", game)

	return game, err
}

func (sqliteConnector *SqliteConnector) SqliteReadAllGames() (*Games, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to read all games from Sqlite database")

	// Query DB rows
	rows, err := sqliteConnector.db.Query("SELECT * FROM soccer_games")
	if err != nil {
		logger.Error().Err(err).Msg("Error reading games from Sqlite database")
		return nil, err
	}
	defer rows.Close()

	// Parse rows into Game struct
	games := []Game{}
	for rows.Next() {
		game := Game{}
		err := rows.Scan(&game.Id, &game.Entity1, &game.Entity2, &game.Entity1Score, &game.Entity2Score)
		if err != nil {
			logger.Error().Err(err).Msg("Error scanning returned Sqlite database row. Occurred during data unmarshalling.")
			return nil, err
		}
		games = append(games, game)
	}

	return &Games{Games: games}, nil
}

func (sqliteConnector *SqliteConnector) SqliteUpdateGame(game Game) (int, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msgf("Attempting to update game in Sqlite Database. Game details passed: %s %s %d %d ID: %d", game.Entity1, game.Entity2, game.Entity1Score, game.Entity2Score, game.Id)
	res, err := sqliteConnector.db.Exec("UPDATE soccer_games SET entity1 = ?, entity2 = ?, entity1_score = ?, entity2_score = ? WHERE id = ?", game.Entity1, game.Entity2, game.Entity1Score, game.Entity2Score, game.Id)
	if err != nil {
		logger.Error().Err(err).Msg("Error executing UPDATE query on Sqlite database")
		return 0, err
	}

	var dbResponseId int64
	if dbResponseId, err = res.RowsAffected(); err != nil {
		logger.Error().Err(err).Msg("Error retrieving number of rows affected by the UPDATE query")
		return 0, err
	}
	return int(dbResponseId), nil
}

func (sqliteConnector *SqliteConnector) SqliteDeleteGame(id int) (int, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msgf("Attempting to delete single game data from Sqlite DB. Game ID Passed: %d", id)
	res, err := sqliteConnector.db.Exec("DELETE FROM soccer_games WHERE id = ?", id)
	if err != nil {
		logger.Error().Err(err).Msg("Error executing DELETE query on Sqlite database")
		return 0, err
	}

	var rowsAffected int64
	if rowsAffected, err = res.RowsAffected(); err != nil {
		logger.Error().Err(err).Msg("Error retrieving number of rows affected by the DELETE query")
		return 0, err
	}

	logger.Info().Msgf("Deleted %d record from Sqlite Database", rowsAffected)

	return int(rowsAffected), nil
}

func (sqliteConnector *SqliteConnector) SqliteDeleteAllGames() (int, error) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msg("Attempting to delete all records in the soccer_games table")
	res, err := sqliteConnector.db.Exec("DELETE FROM soccer_games")
	if err != nil {
		logger.Error().Err(err).Msg("Error deleting all records in the soccer_games table")
		return 0, err
	}

	var rowsAffected int64
	if rowsAffected, err = res.RowsAffected(); err != nil {
		logger.Error().Err(err).Msg("Error deleting all records in soccer_games table")
		return 0, err
	}

	logger.Info().Msgf("Delete %d records from Sqlite Database", rowsAffected)

	return int(rowsAffected), nil
}
