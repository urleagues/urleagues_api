package games

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"sort"
	"strings"
	"testing"

	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"gitlab.com/urleagues_api/app_features/data_management"
	. "gitlab.com/urleagues_api/app_features/data_management/data_models"
)

func mainTesting(m *testing.M) int {
	tempDir, setupErr := data_management.TestSetup()
	defer os.RemoveAll(tempDir)
	log.Printf("Tempdir: %s", tempDir)
	if setupErr != nil {
		log.Fatalf("Error setting up testing database: %s", setupErr)
	}

	return m.Run()
}

func TestMain(m *testing.M) {
	os.Exit(mainTesting(m))
}

func getNewRequest(t *testing.T, requestMethod, path string, payload io.Reader) *http.Request {
	req, err := http.NewRequest(strings.ToUpper(requestMethod), path, payload)
	if err != nil {
		t.Errorf("Error generating new request for tests: %v", err)
	}

	return req
}

type route struct {
	pattern      string
	handlerVerbs []string
}

func TestRoutes(t *testing.T) {
	tests := []struct {
		name           string
		expectedResult []route
	}{
		{
			name: "Happy Path",
			expectedResult: []route{
				{
					pattern:      "/",
					handlerVerbs: []string{"GET", "POST"},
				},
				{
					pattern:      "/{gameID}",
					handlerVerbs: []string{"DELETE", "GET", "PUT"},
				},
			},
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			router := Routes()
			routes := router.Routes()

			for i := 0; i < len(routes); i++ {
				if routes[i].Pattern != testStruct.expectedResult[i].pattern {
					t.Errorf("Routes returned wrong route pattern: got %v want %v",
						routes[i].Pattern, testStruct.expectedResult[i].pattern)
				}

				keys := make([]string, len(routes[i].Handlers))
				l := 0
				for k := range routes[i].Handlers {
					keys[l] = k
					l++
				}
				sort.Strings(keys)

				for x := 0; x < len(keys); x++ {
					if keys[x] != testStruct.expectedResult[i].handlerVerbs[x] {
						t.Errorf("Routes returned wrong route handler verb: got %v want %v",
							keys[x], testStruct.expectedResult[i].handlerVerbs[x])
					}
				}
			}
		})
	}
}

func TestCreateGame(t *testing.T) {
	defer data_management.TestDatabaseReset(4, t)

	var POST_VALUES Games = Games{
		Games: []Game{
			{
				Entity1:      "peach",
				Entity2:      "daisy",
				Entity1Score: 7,
				Entity2Score: 6,
			},
		},
	}

	postBodies := [][]byte{}

	for _, game := range POST_VALUES.Games {
		postBody, err := json.Marshal(game)
		if err != nil {
			t.Errorf("Error marshalling json post values into []byte")
		}
		postBodies = append(postBodies, postBody)
	}

	tests := []struct {
		name           string
		req            *http.Request
		res            *httptest.ResponseRecorder
		contentType    string
		expectedResult string
	}{
		{
			name:           "Happy Path",
			req:            getNewRequest(t, "POST", "/", bytes.NewBuffer(postBodies[0])),
			res:            httptest.NewRecorder(),
			contentType:    "application/json",
			expectedResult: "{\"id\":4,\"entity1\":\"peach\",\"entity2\":\"daisy\",\"entity1Score\":7,\"entity2Score\":6}\n",
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			handler := http.HandlerFunc(CreateGame)
			testStruct.req.Header.Add("Content-Type", testStruct.contentType)
			handler.ServeHTTP(testStruct.res, testStruct.req)

			assert := assert.New(t)

			assert.Equal(http.StatusOK, testStruct.res.Code)
			assert.Equal(testStruct.expectedResult, testStruct.res.Body.String())
		})
	}
}

func TestGetSingleGame(t *testing.T) {
	tests := []struct {
		name           string
		req            *http.Request
		res            *httptest.ResponseRecorder
		expectedResult string
	}{
		{
			name:           "Happy Path",
			req:            getNewRequest(t, "GET", "/1", nil),
			res:            httptest.NewRecorder(),
			expectedResult: "{\"id\":1,\"entity1\":\"mario\",\"entity2\":\"bowser\",\"entity1Score\":4,\"entity2Score\":3}\n",
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			handler := http.HandlerFunc(GetSingleGame)

			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("gameID", "1")

			ctx := context.WithValue(testStruct.req.Context(), chi.RouteCtxKey, rctx)
			testStruct.req = testStruct.req.WithContext(ctx)
			handler.ServeHTTP(testStruct.res, testStruct.req)

			assert := assert.New(t)

			assert.Equal(http.StatusOK, testStruct.res.Code)
			assert.Equal(testStruct.expectedResult, testStruct.res.Body.String())
		})
	}
}

func TestGetAllGames(t *testing.T) {
	tests := []struct {
		name           string
		req            *http.Request
		res            *httptest.ResponseRecorder
		expectedResult string
	}{
		{
			name:           "Happy Path",
			req:            getNewRequest(t, "GET", "/", nil),
			res:            httptest.NewRecorder(),
			expectedResult: "{\"games\":[{\"id\":1,\"entity1\":\"mario\",\"entity2\":\"bowser\",\"entity1Score\":4,\"entity2Score\":3},{\"id\":2,\"entity1\":\"luigi\",\"entity2\":\"wario\",\"entity1Score\":5,\"entity2Score\":2},{\"id\":3,\"entity1\":\"yoshi\",\"entity2\":\"waluigi\",\"entity1Score\":3,\"entity2Score\":1}]}\n",
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			handler := http.HandlerFunc(GetAllGames)

			handler.ServeHTTP(testStruct.res, testStruct.req)

			assert := assert.New(t)

			assert.Equal(http.StatusOK, testStruct.res.Code)
			assert.Equal(testStruct.expectedResult, testStruct.res.Body.String())
		})
	}
}

func TestUpdateGame(t *testing.T) {
	defer data_management.TestDatabaseReset(3, t)

	var PUT_VALUES Games = Games{
		Games: []Game{
			{
				Entity1:      "tim",
				Entity2:      "mini-bowser",
				Entity1Score: 3,
				Entity2Score: 2,
			},
		},
	}

	putBodies := [][]byte{}

	for _, game := range PUT_VALUES.Games {
		putBody, err := json.Marshal(game)
		if err != nil {
			t.Errorf("Error marshalling json post values into []byte")
		}
		putBodies = append(putBodies, putBody)
	}

	tests := []struct {
		name           string
		req            *http.Request
		res            *httptest.ResponseRecorder
		contentType    string
		expectedResult string
	}{
		{
			name:           "Happy Path",
			req:            getNewRequest(t, "PUT", "/2", bytes.NewBuffer(putBodies[0])),
			res:            httptest.NewRecorder(),
			contentType:    "application/json",
			expectedResult: "{\"id\":2,\"entity1\":\"tim\",\"entity2\":\"mini-bowser\",\"entity1Score\":3,\"entity2Score\":2}\n",
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			handler := http.HandlerFunc(UpdateGame)

			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("gameID", "2")

			ctx := context.WithValue(testStruct.req.Context(), chi.RouteCtxKey, rctx)
			testStruct.req = testStruct.req.WithContext(ctx)
			testStruct.req.Header.Add("Content-Type", testStruct.contentType)

			handler.ServeHTTP(testStruct.res, testStruct.req)

			assert := assert.New(t)

			assert.Equal(http.StatusOK, testStruct.res.Code)
			assert.Equal(testStruct.expectedResult, testStruct.res.Body.String())
		})
	}
}

func TestDeleteGame(t *testing.T) {
	defer data_management.TestDatabaseReset(2, t)
	tests := []struct {
		name           string
		req            *http.Request
		res            *httptest.ResponseRecorder
		expectedResult string
	}{
		{
			name:           "Happy Path",
			req:            getNewRequest(t, "DELETE", "/3", nil),
			res:            httptest.NewRecorder(),
			expectedResult: "{\"status\":\"Deleted game successfully\"}\n",
		},
	}

	for _, testStruct := range tests {
		t.Run(testStruct.name, func(t *testing.T) {
			handler := http.HandlerFunc(DeleteGame)

			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("gameID", "3")

			ctx := context.WithValue(testStruct.req.Context(), chi.RouteCtxKey, rctx)
			testStruct.req = testStruct.req.WithContext(ctx)

			handler.ServeHTTP(testStruct.res, testStruct.req)

			assert := assert.New(t)

			assert.Equal(http.StatusOK, testStruct.res.Code)
			assert.Equal(testStruct.expectedResult, testStruct.res.Body.String())
		})
	}
}
