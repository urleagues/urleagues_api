package games

import (
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/urleagues_api/app_features/data_management"
	"gitlab.com/urleagues_api/app_features/data_management/data_models"
	"gitlab.com/urleagues_api/app_features/error_handling"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/urleagues_api/app_features/logging"
)

// Routes is a function for easily receiving all of the routes from this package
func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Post("/", CreateGame)
	router.Get("/{gameID}", GetSingleGame)
	router.Get("/", GetAllGames)
	router.Put("/{gameID}", UpdateGame)
	router.Delete("/{gameID}", DeleteGame)
	return router
}

// CreateGame handles the POST to the api for creating game objects
func CreateGame(w http.ResponseWriter, r *http.Request) {
	logger := logging.DefaultConfigLogger()
	postedGame := &data_models.Game{}

	if err := render.Bind(r, postedGame); err != nil {
		render.Render(w, r, error_handling.ErrInvalidRequest(err))
		return
	}

	insertId, err := data_management.InsertGame(*postedGame)
	if err != nil {
		logger.Error().Msg("Failed to add game to data storage")
		render.Render(w, r, error_handling.ErrInternalServerError())
		return
	}

	postedGame.Id = insertId
	render.JSON(w, r, postedGame)
}

// GetSingleGame handles the GET for a single game object
func GetSingleGame(w http.ResponseWriter, r *http.Request) {
	logger := logging.DefaultConfigLogger()
	logger.Info().Msgf("Incoming path url: %s", r.URL)
	gameID := chi.URLParam(r, "gameID")
	logger.Info().Msgf("Received gameID from the URL Param: %s", gameID)
	gameIDInt, err := strconv.Atoi(gameID)
	if err != nil {
		logger.Error().Err(err).Msg("Error converting gameID URL parameter from string to integer")
		render.Render(w, r, error_handling.ErrInvalidRequest(errors.New("Error converting gameID URL parameter from string to integer. Check request and retry.")))
		return
	}

	game, err := data_management.ReadGame(gameIDInt)
	if err != nil {
		logger.Error().Msg("Error reading game from data storage")
		render.Render(w, r, error_handling.ErrInternalServerError())
		return
	}
	render.JSON(w, r, game) // Helper for serializing and returning JSON
}

// GetAllGames handles the GET for all of the game objects
func GetAllGames(w http.ResponseWriter, r *http.Request) {
	logger := logging.DefaultConfigLogger()
	games, err := data_management.ReadGames()
	if err != nil {
		logger.Error().Msg("Failed to read games from data storage")
		render.Render(w, r, error_handling.ErrInternalServerError())
		return
	}

	render.JSON(w, r, games)
}

// UpdateGame handles the PUT for updating a game object
func UpdateGame(w http.ResponseWriter, r *http.Request) {
	logger := logging.DefaultConfigLogger()
	gameID := chi.URLParam(r, "gameID")
	gameIDInt, err := strconv.Atoi(gameID)
	if err != nil {
		logger.Error().Err(err).Msg("Error converting gameID URL parameter from string to integer")
		render.Render(w, r, error_handling.ErrInvalidRequest(errors.New("Error converting gameID URL parameter from string to integer. Check request and retry.")))
		return
	}

	putGame := &data_models.Game{}

	if err := render.Bind(r, putGame); err != nil {
		logger.Error().Err(err).Msg("Error unmarshalling PUT request into Go struct")
		render.Render(w, r, error_handling.ErrInvalidRequest(err))
		return
	}

	putGame.Id = gameIDInt

	update_err := data_management.UpdateGame(*putGame)
	if update_err != nil {
		logger.Error().Msg("Failed to update game in data storage")
		render.Render(w, r, error_handling.ErrInternalServerError())
		return
	}

	render.JSON(w, r, putGame)
}

// DeleteGame handles the DELETE for removing a game object from the database
func DeleteGame(w http.ResponseWriter, r *http.Request) {
	logger := logging.DefaultConfigLogger()
	gameID := chi.URLParam(r, "gameID")
	gameIDInt, err := strconv.Atoi(gameID)
	if err != nil {
		logger.Error().Err(err).Msg("Error converting gameID URL parameter from string to integer")
		render.Render(w, r, error_handling.ErrInvalidRequest(errors.New("Error converting gameID URL parameter from string to integer. Check request and retry.")))
		return
	}

	delete_err := data_management.DeleteGame(gameIDInt)
	if delete_err != nil {
		logger.Error().Msg("Failed to delete game in data storage")
		render.Render(w, r, error_handling.ErrInternalServerError())
		return
	}

	response := make(map[string]string)
	response["status"] = "Deleted game successfully"
	render.JSON(w, r, response)
}
