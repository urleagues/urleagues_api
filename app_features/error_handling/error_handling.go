package error_handling

import (
	"net/http"

	"github.com/go-chi/render"
)

// Using the error handling techniques that go-chi uses in their http server example: https://github.com/go-chi/chi/blob/master/_examples/rest/main.go
// The error responses have to implement the Render method in order to be of type render.Renderer so that Go-Chi can marshall them to be sent to the client.

type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "Invalid request.",
		ErrorText:      err.Error(),
	}
}

func ErrInternalServerError() render.Renderer {
	return &ErrResponse{
		HTTPStatusCode: 500,
		StatusText:     "Internal Server Error Occurred",
	}
}
