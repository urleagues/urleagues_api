package environment_management

import (
	"os"

	"github.com/joho/godotenv"
)

func LoadEnvironmentVariables() {
	env := os.Getenv("URLEAGUES_ENV")
	if env == "" {
		env = "development"
	}

	godotenv.Load(".env." + env + ".local")
	if env != "test" {
		godotenv.Load(".env.local")
	}
	godotenv.Load(".env." + env)
	godotenv.Load() // The Original .env
}

func ReadFromEnv(key, backupValue string) string {
	envValue := os.Getenv(key)

	if envValue == "" {
		envValue = backupValue
	}

	return envValue
}
