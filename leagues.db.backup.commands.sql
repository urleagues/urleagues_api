.open leagues.db
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS [soccer_games] (
id INTEGER NOT NULL PRIMARY KEY,
entity1 TEXT NOT NULL,
entity2 TEXT NOT NULL,
entity1_score INTEGER NOT NULL,
entity2_score INTEGER NOT NULL
);
INSERT INTO "soccer_games" VALUES
	(NULL, 'Tim','Luigi', 7, 6),
	(NULL, 'Tim','Mario', 7, 2),
	(NULL, 'Tim','Mario', 3, 0);
COMMIT;
